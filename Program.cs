using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Diagnostics;
using Microsoft.Data.Sqlite;

namespace SQLite.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();

                    //Add a new product using ADO.NET
                    try
                    {
                        AddProductUsingAdo("Dremel Tool");

                    }
                    catch (System.Exception)
                    {
                        //eat the the error
                    }


                    //Get the products using ADO.NET
                    DisplayProductsUsingAdo();

                    //add a new product using EF
                    try
                    {
                        AddNewProduct("Tile");

                    }
                    catch (System.Exception)
                    {
                        //eat it
                    }

                    DisplayProductsUsingEF();



                });


        #region ADO.Net
        private static void DisplayProductsUsingAdo()
        {
            SqliteConnection conn = new SqliteConnection(@"Data Source=DB\ChampionProductsDB.db;");
            conn.Open();

            SqliteDataReader datareader;
            SqliteCommand command;

            using (conn)
            {
                command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM product";

                datareader = command.ExecuteReader();
                while (datareader.Read())
                {
                    string product = datareader.GetString(1);
                    Debug.WriteLine(product);
                }

            }
        }


        private static bool AddProductUsingAdo(string ProductName)
        {
            SqliteConnection conn = new SqliteConnection(@"Data Source=DB\ChampionProductsDB.db;");
            conn.Open();

            SqliteDataReader datareader;
            SqliteCommand command;


            using (conn)
            {
                command = conn.CreateCommand();
                command.CommandText = "Insert into product (Description) Values ('" + ProductName + "')";
                command.ExecuteNonQuery();

            }

            return true;

        }
        #endregion


        #region EF
        public class Product
        {
            [Key]
            public int ProductId { get; set; }
            public string Description { get; set; }
        }

        public class ChampionContext : DbContext
        {
            public DbSet<Product> Products { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=DB\ChampionProductsDB.db;");

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                modelBuilder.Entity<Product>().ToTable("Product");
            }
        }


        public static void AddNewProduct(string ProductName)
        {
            using (var db = new ChampionContext())
            {
                var newProduct = new Product()
                {
                    Description = ProductName
                };

                db.Products.Add(newProduct);
                db.SaveChanges();
            }
        }

        public static void DisplayProductsUsingEF()
        {
            using (var db = new ChampionContext())
            {
                var products = db
                                .Products
                                .AsNoTracking();

                foreach (var item in products)
                {
                    Debug.WriteLine(item.Description);
                }
            }
        }

        #endregion

    }

}